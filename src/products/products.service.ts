import { Injectable } from "@nestjs/common";
import { Product } from './product.model';

@Injectable ()
export class ProductsService {
    products: Product[]= [];

    insertProduct(title: string, desc: string, price: number) {
        const productId = new Date().toString();
        const newProduct = new Product(productId, title, desc, price);
        this.products.push(newProduct);
        return productId;
    }

   
    // getProduct() {

    // }
}