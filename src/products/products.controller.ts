import { Controller, Body, Post, Get } from "@nestjs/common";
import { ProductsService } from './products.service';
@Controller('products')
export class ProductsController {
    constructor(private readonly productsService: ProductsService){}
    @Post()
    addProduct(@Body() product: {title: string, desc: string, price: number}): any {
        const generatedId = this.productsService.insertProduct(product.title, product.desc, product.price);
        return {id: generatedId};
    }
}