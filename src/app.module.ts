import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProductModule } from './products/products.module';
//import { ConfigModule } from './config/config.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { IdeadModule } from './idea/idea.module';
import { APP_FILTER } from '@nestjs/core';
import { HttpErrorFilter } from './shared/http-error.filter';
import { UserModule } from './user/user.module';
//import { MassiveModule } from '@nestjsplus/massive';
//import { ConfigService } from './config/config.service';
import { CommentModule } from './comment/comment.module';

@Module({
  imports: [ProductModule,
    IdeadModule,
    UserModule,
    TypeOrmModule.forRoot({
      autoLoadEntities: true
    }
    ),
    CommentModule,
  ],
  controllers: [AppController],
  providers: [AppService, {
    provide: APP_FILTER,
    useClass: HttpErrorFilter,
  },
],
})
export class AppModule {}
