import { Controller, Get, Param, Post, UseGuards, UsePipes, Body } from '@nestjs/common';
import { CommentService } from './comment.service';
import { AuthGuard } from 'src/shared/auth.guard';
import {ValidationPipe} from '../shared/validation.pipe'
import { User } from 'src/user/user.decorator';
import { CommentDTO } from './comment.dto';

@Controller('api/comment')
export class CommentController {
    constructor(private commentService: CommentService) {}

    @Get('idea/:id')
    getCommentByIdea(@Param('id') id:string) {
        return this.commentService.showByIdea(id);
    }

    @Get(':id')
    showCommentById(@Param('id') id: string) {
        return this.commentService.show(id);
    }
    @Get('user/:id')
    getCommentByUser(@Param('id') id:string) {
      return this.commentService.showByUser(id);
    }

    @Post('idea/:id')
    @UseGuards(new AuthGuard())
    @UsePipes(new ValidationPipe())
    createComment(@Param('id') id:string, @User('id') userId:string, @Body() data: CommentDTO) {
        try {
            return this.commentService.create(id, userId, data);
        } catch (error) {
            console.log(error);
        }
    }
}
