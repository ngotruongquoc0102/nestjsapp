import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CommentEntity } from 'src/entities/comment.entity';
import { Repository } from 'typeorm';
import { UserEntity } from 'src/entities/user.entity';
import { IdeaEntity } from 'src/entities/idea.entity';
import { CommentDTO } from './comment.dto';

@Injectable()
export class CommentService {
 constructor(
     @InjectRepository(CommentEntity) private commentRepository:Repository<CommentEntity>,
     @InjectRepository(UserEntity) private userRepository:Repository<UserEntity>,
     @InjectRepository(IdeaEntity) private ideaRepository:Repository<IdeaEntity>
 ) {}
async show(id:string) {
     const comment = await this.commentRepository.findOne({where: {id}, relations: ['author', 'idea']});
    return comment; 
}
async create(id: string, userId: string, data: CommentDTO) {
    try {
        const idea = await this.ideaRepository.findOne({where: {id}});
    
    const user = await this.userRepository.findOne({where: {id: userId}});
    if(!idea) {
        throw new HttpException('Idea Not Found', HttpStatus.BAD_REQUEST);
    }
    const comment = await this.commentRepository.create({
        ...data,
        idea,
        author: user
    });
    this.commentRepository.save(comment);
    return comment;
    } catch (error) {
        console.log(error);
    }
}
async showByIdea(id: string) {
    const idea = await this.ideaRepository.findOne({where: {id}, relations: ['comments', 'comments.author']});
    return idea;
}
async showByUser(id: string) {
    //const comment = await this.commentRepository.find({where: {author: id}, relations: ['author', 'idea']}); 
    const comment = await this.userRepository.findOne({where: {id}, relations: ['comments', 'comments.idea']})
    return comment.toResponseObject();
    
}
}
