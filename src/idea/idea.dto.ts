import { ApiProperty } from '@nestjs/swagger'
import { UserRO } from "src/user/user.dto";
export class IdeaDTO {
    @ApiProperty({
        type: String,
    })
    readonly idea: string;
    @ApiProperty({
        type: String,
    })
    readonly description: string;
}

export class IdeaRO {
    id: string;
    idea: string;
    description: string;
    created: Date;
    updated: Date;
    author: UserRO;
    upvote?: UserRO;
    downvote?: UserRO;
}