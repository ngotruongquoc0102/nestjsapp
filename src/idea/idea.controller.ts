import { Controller, Get, Post, Put, Delete, Body, Param, UsePipes, UseGuards} from "@nestjs/common";
import { IdeaService } from './idea.service';
import { IdeaDTO } from "./idea.dto";
import { ValidationPipe } from '../shared/validation.pipe';
import { AuthGuard } from "src/shared/auth.guard";
import { UserEntity } from "src/entities/user.entity";
import { User } from '../user/user.decorator';
import { Logger } from '@nestjs/common';
import { UserDTO } from "src/user/user.dto";
import {ApiBearerAuth} from '@nestjs/swagger';
interface LabeledValue {
    size: number;
    label: string;
}
@Controller('idea')
export class IdeaController {
    myObj = {size: 10, label: "Size 10 Object"};
    constructor(private ideaService: IdeaService){}
    printLabel(labeledObj: LabeledValue) {
        console.log(labeledObj);
    }
    private logData(options: any) {
        options.user && Logger.log('BODY'+ JSON.stringify(options.user));
        options.data && Logger.log('DATA', JSON.stringify(options.data));
        options.id && Logger.log('ID', JSON.stringify(options.id));
         
    }
    @Get()
    @UseGuards(new AuthGuard())
    @ApiBearerAuth()
    showAllIdeas() {
        return this.ideaService.showAll();
    }
   
    @Post()
    @UseGuards(new AuthGuard())
    @UsePipes(new ValidationPipe())
    @ApiBearerAuth()
    createIdea(@User('id') user, @Body() data: IdeaDTO) {
        return this.ideaService.createOne(user, data);
    }

    @Get(':id')
    readIdea(@Param('id') id: string) {
        return this.ideaService.read(id);
    }
    @Put(':id')
    @UseGuards(new AuthGuard())
    @ApiBearerAuth()
    updateIdea(@Param('id') id: string, @User('id') user, @Body() data: Partial<IdeaDTO>) {
        return this.ideaService.update(id, user, data);
    }

    @Delete(':id')
    destroyIdea(@Param('id') id: string, @User('id') user) {
        return this.ideaService.destroy(id, user);
    }

    @Post(':id/bookmark')
    @UseGuards(new AuthGuard)
    @ApiBearerAuth()
    bookmarkIdea(@Param('id') id: string, @User('id') user: string) {
        this.logData({id, user});
        return this.ideaService.bookmark(id, user);
    }
    
    @Delete(':id/unbookmark')
    @UseGuards(new AuthGuard())
    @ApiBearerAuth()
    unbookmarkIdea(@Param('id') id: string, @User('id') user: string) {
       try {
        this.logData({id, user});
        return this.ideaService.unbookmark(id, user);
       } catch (error) {
           console.log(error);
       }
    }

    @Post(':id/upvote')
    @UseGuards(new AuthGuard())
    @ApiBearerAuth()
    upvoteIdea(@Param('id') id:string, @User('id') user: string) {
        this.logData({id, user})
        return this.ideaService.vote(id, user);
    }
    @Post(':id/downvote')
    @UseGuards(new AuthGuard())
    @ApiBearerAuth()
    downvoteIdea(@Param('id') id:string, @User('id') user: string) {
        this.logData({id, user});
        return this.ideaService.unvote(id, user);
    }

}