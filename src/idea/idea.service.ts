import { Injectable, HttpException, HttpStatus } from "@nestjs/common";
import { Repository } from "typeorm";
import { IdeaEntity } from "src/entities/idea.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { IdeaDTO, IdeaRO } from './idea.dto';
import { UserEntity } from "src/entities/user.entity";
import { Votes } from "src/shared/votes.enum";

@Injectable()
export class IdeaService {
    constructor(
    @InjectRepository(IdeaEntity)
     private ideaRepository: Repository<IdeaEntity>,
     @InjectRepository(UserEntity)
     private userRepository: Repository<UserEntity>,   
     ) {}

    async showAll(): Promise<IdeaRO[]> {
        const ideas =  await this.ideaRepository.find({relations: ['author', 'downvotes', 'upvotes']});
        
        return ideas.map(idea => this.toResponseObject(idea));
    }

    async createOne(userId: string, data: IdeaDTO) {
        const user =  await this.userRepository.findOne({where: {id: userId}});
        const idea = await this.ideaRepository.create({...data, author:user});
        await this.ideaRepository.save(idea);
        return this.toResponseObject(idea);
    }

    async read(id: string): Promise<IdeaRO> {
        try {
            const idea = await this.ideaRepository.findOne({where: {id}, relations: ['author']})
            return this.toResponseObject(idea);
        } catch (error) {
           throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
    } 

    async update(id: string, user: string, data: Partial<IdeaDTO>): Promise<IdeaRO> {
        try {
            let idea = await this.ideaRepository.findOne({where: {id}, relations: ['author']}); 
            if(!idea) {
                throw new HttpException('Not found', HttpStatus.NOT_FOUND);
            }
            console.log('idea', user);
            this.EnsureOwnerShip(idea, user);
            console.log('idea');
            await this.ideaRepository.update({id}, data);
            idea = await this.ideaRepository.findOne({where: {id}, relations: ['author']});
          
            
            return this.toResponseObject(idea);
        } catch (error) {
            throw new HttpException('Not Accepted', HttpStatus.NOT_ACCEPTABLE);
        }
    }

    async destroy(id: string, user: string): Promise<IdeaRO> {
        try {
            const idea = await this.ideaRepository.findOne({where: {id}, relations: ['author']});
            if(!idea) {
                throw new HttpException('Not Found', HttpStatus.NOT_FOUND);
            }
            this.EnsureOwnerShip(idea, user);
            await this.ideaRepository.delete({id});
            return this.toResponseObject(idea);
        } catch (error) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
    }

    async bookmark(id: string, userId: string) {
        const idea = await this.ideaRepository.findOne({where: {id}});
        const user = await this.userRepository.findOne({where: {id: userId}, relations: ['booksmarks']});

        if(user.booksmarks.filter(bookmark => bookmark.id === idea.id).length < 1) {
            user.booksmarks.push(idea);
            await this.userRepository.save(user);
        } else {
            throw new HttpException(
                'idea already bookmarked',
                HttpStatus.BAD_REQUEST,
            );
        }
        
        return user.toResponseObject();
    }

    async unbookmark(id: string, userId: string) {
        const idea = await  this.ideaRepository.findOne({where: {id}});
        const user = await this.userRepository.findOne({where: {id: userId}, relations: ['booksmarks']});
        
        if(user.booksmarks.filter(bookmark => bookmark.id === idea.id).length > 0) {
            user.booksmarks =  user.booksmarks.filter(bookmark => bookmark.id !== idea.id);
            await this.userRepository.save(user);
        } else {
            throw new HttpException(
                'idea has bookmarked yet',
                HttpStatus.BAD_REQUEST,
            );
        }
        return user.toResponseObject();
    }
    private  toResponseObject(idea: IdeaEntity): IdeaRO {
        return {...idea, author: idea.author.toResponseObject(false)};
    }

    private EnsureOwnerShip(idea: IdeaEntity, author: string) {
        //check if Idea Author and UserID are equal
        if(idea.author.id !== author) {
            throw new HttpException('Incorrect User', HttpStatus.UNAUTHORIZED);
        }
    }

    private async voteIdea(idea: IdeaEntity, user: UserEntity, vote: Votes) {
        const opposite = vote === Votes.UP? Votes.DOWN : Votes.UP;
         if(
            idea[opposite].filter(voter => voter.id === user.id).length > 0 ||
            idea[vote].filter(voter => voter.id === user.id).length > 0
         ) {
            idea[opposite] = idea[opposite].filter(vote => vote.id !== user.id);
            idea[vote] = idea[vote].filter(vote => vote.id !== user.id);
            
            await this.ideaRepository.save(idea);
         }
         else if(idea[vote].filter(vote => vote.id === user.id).length <1) {
            idea[vote].push(user)
            await this.ideaRepository.save(idea);
         }
         else {
             throw new HttpException('Unable to cast vote', HttpStatus.BAD_REQUEST);
         }
         return idea;
    }

    async unvote(id: string, userId: string) {
        let idea = await this.ideaRepository.findOne({where: {id}, relations: ['author', 'downvotes', 'upvotes']});
        const user = await this.userRepository.findOne({where: {id: userId}});
        idea = await this.voteIdea(idea, user, Votes.DOWN);
        
        return this.toResponseObject(idea);
    }

    async vote(id: string, userId: string) {
        let idea = await this.ideaRepository.findOne({where: {id}, relations: ['author', 'downvotes', 'upvotes']});
        const user = await this.userRepository.findOne({where: {id: userId}});
        
         idea = await this.voteIdea(idea, user, Votes.UP);
        return this.toResponseObject(idea);
    }
}