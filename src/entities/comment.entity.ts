import { Entity, PrimaryGeneratedColumn, CreateDateColumn, Column, ManyToMany, ManyToOne, JoinTable } from "typeorm";
import { type } from "os";
import { UserEntity } from "./user.entity";
import { IdeaEntity } from "./idea.entity";

@Entity('comment')
export class CommentEntity {
@PrimaryGeneratedColumn('uuid')
id: string;
@CreateDateColumn()
created: Date;

@Column('text')
comment: string;

@ManyToOne(type => UserEntity, author => author.comments)
author: UserEntity;

@ManyToOne(type => IdeaEntity, idea => idea.comments)
idea: IdeaEntity
}