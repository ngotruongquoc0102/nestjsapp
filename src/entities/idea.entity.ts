import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, ManyToOne, UpdateDateColumn, JoinTable, ManyToMany, OneToMany } from 'typeorm';
import { UserEntity } from './user.entity';
import { CommentEntity } from './comment.entity';

@Entity('idea')
export class IdeaEntity {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column('text') idea: string;

    @Column('text') description: string;

    @CreateDateColumn()
    created: Date;

    @UpdateDateColumn()
    updated: Date;
    
    @ManyToOne(type => UserEntity, author => author.ideas)
    author: UserEntity

    @ManyToMany(type => UserEntity, {cascade: true})
    @JoinTable()
    upvotes: UserEntity[]; 
    @ManyToMany(type => UserEntity, {cascade: true})
    @JoinTable()
    downvotes: UserEntity[]; 
  
    @OneToMany(type => CommentEntity, comment => comment.idea)
    @JoinTable()
    comments: CommentEntity[]
}