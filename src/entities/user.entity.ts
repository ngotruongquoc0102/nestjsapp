import { Entity, PrimaryGeneratedColumn, CreateDateColumn, Column, BeforeInsert, OneToMany, ManyToMany, JoinTable } from "typeorm";
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import { response } from "express";
import { UserRO } from "src/user/user.dto";
import { IdeaEntity } from "./idea.entity";
import { CommentEntity } from "./comment.entity";

@Entity('user')
export class UserEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @CreateDateColumn()
    created: Date;

    @Column({
        type: 'text',
        unique: true,
    })
    username: string;

    @Column({
        type: 'text',
    })
    password: string;
    
    @OneToMany(type => IdeaEntity, idea => idea.author, {cascade: true})
    ideas: IdeaEntity[];
    
    @ManyToMany(type => IdeaEntity, {cascade: true})
    @JoinTable()
    booksmarks: IdeaEntity[];
    @BeforeInsert()
    async hashPassword() {
        this.password = await bcrypt.hash(this.password, 12);
    }

    @OneToMany(type => CommentEntity, comment => comment.author, {cascade: true})
    comments: CommentEntity[];


    toResponseObject(showToken = true): UserRO {
        const {id, created, username, token} = this;
        const responseObject: any = {id, created, username};
        if(showToken) {
            responseObject.token = token;
        }
        if(this.ideas) {
            responseObject.ideas = this.ideas;
        }
        if(this.booksmarks) {
            responseObject.bookmarks = this.booksmarks;
        }
        if(this.comments) {
            responseObject.comments = this.comments;
        }
        return responseObject;
    }
    
    async comparePassword(attempt: string) {
        return await bcrypt.compare(attempt, this.password);
    }

    private get token(){
        const {id, username} = this;
        return jwt.sign({id, username}, process.env.SECRET, {expiresIn: '7d'});
    }
}