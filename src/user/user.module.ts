import { Module } from "@nestjs/common";
import { UserController } from "./user.controller";
import { UserService } from "./user.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UserEntity } from "src/entities/user.entity";
import { IdeaEntity } from "src/entities/idea.entity";

@Module({
    imports: [TypeOrmModule.forFeature([UserEntity, IdeaEntity])],
    controllers: [UserController],
    providers: [UserService],
}) export class UserModule {}