import { Injectable, HttpException, HttpStatus } from "@nestjs/common";
import { UserEntity } from '../entities/user.entity';
import { Repository } from "typeorm";
import { InjectRepository } from '@nestjs/typeorm';
import { UserDTO, UserRO } from "./user.dto";
import { Pagination } from './Pagination.dto';
import { PaginationUserResult } from "./PaginationUserResult.dto";
@Injectable()
export class UserService {
    constructor(
        @InjectRepository(UserEntity) private userRepository: Repository<UserEntity>,
    ){}

    async showAll(userPaginationDTO: Pagination): Promise<PaginationUserResult> {
       try {
        //const users = await this.userRepository.find({relations:['ideas']});
        //return users.map(user => user.toResponseObject(false));
        
        //Number item to skip
        const skippedItem = (userPaginationDTO.page-1)*(userPaginationDTO.limit);
        
        const totalCount = await this.userRepository.count();

        const users = await this.userRepository.createQueryBuilder()
        .orderBy('created', 'DESC')
        .offset(skippedItem)
        .limit(userPaginationDTO.limit)
        .getMany();
        return {
            totalCount,
            page: userPaginationDTO.page,
            limit: userPaginationDTO.limit,
            data: users.map(user => user.toResponseObject(false)),
        };
        
       } catch (error) {
           console.log(error);
           throw new HttpException(
               'Error with create user',
               HttpStatus.BAD_REQUEST,
           );
       }

    }

    async login(data: UserDTO): Promise<UserRO> {
     const {username, password} = data;
     const user = await this.userRepository.findOne({where: {username}});
     if(!user || !await user.comparePassword(password)) {
        throw new HttpException(
            'Invalid username/password',
            HttpStatus.BAD_REQUEST,
        );
     }
     return user.toResponseObject(true);
    }

    async register(data: UserDTO): Promise<UserRO> {
        const { username } = data;
        console.log('here');
        
        let user = await this.userRepository.findOne({where: {username}});
        if(user) {
            console.log('here');
            throw new HttpException(
                'User already exist',
                HttpStatus.BAD_REQUEST,
            );
        } 
        user = await this.userRepository.create(data);
        await this.userRepository.save(user);
        return user.toResponseObject();
    }
}