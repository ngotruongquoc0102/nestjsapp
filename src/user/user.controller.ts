import { Controller, Get, Post, Body, UsePipes, UseGuards, Query } from "@nestjs/common";
import { UserDTO } from "./user.dto";
import {ValidationPipe} from '../shared/validation.pipe'
import { UserService } from './user.service';
import { AuthGuard } from "src/shared/auth.guard";
import { User } from "./user.decorator";
import { Pagination } from "./Pagination.dto";
import { ApiCreatedResponse, ApiOkResponse, ApiUnauthorizedResponse, ApiProduces, ApiBody, ApiBearerAuth } from '@nestjs/swagger';
@Controller('user')
export class UserController {
    constructor(private userService: UserService) {}
    @Get('api/users')
    @UseGuards(new AuthGuard())
    @ApiBearerAuth()
    getAllUsers(@Query() paginationDTO: Pagination) {
        paginationDTO.limit = Number(paginationDTO.limit);
        paginationDTO.page = Number(paginationDTO.page);

        return this.userService.showAll({
            ...paginationDTO,
            limit: paginationDTO.limit > 10 ? 10 : paginationDTO.limit
        });
    }
    @Post('login')
    @ApiOkResponse({ description: 'user login'})
    @ApiUnauthorizedResponse({description: 'Invalids credentials'})
    @UsePipes(new ValidationPipe())
    login(@Body() data: UserDTO) {
        return this.userService.login(data);
    }
    @Post('register')
    @ApiCreatedResponse({description: 'User registration'})
    @UsePipes(new ValidationPipe())
    @ApiBody({type: UserDTO})
    register(@Body() data: UserDTO) {
        return this.userService.register(data);
    }
}