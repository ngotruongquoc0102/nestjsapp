import { UserEntity } from '../entities/user.entity';
import { ApiProperty } from '@nestjs/swagger';
import { UserRO } from './user.dto';
export class PaginationUserResult {
    data: UserRO[];
    
    @ApiProperty({
        type: Number,
    })
    limit: number;
    @ApiProperty({
        type: Number,
    })
    totalCount: number;
    @ApiProperty({
        type: Number,
    })
    page: number;
}