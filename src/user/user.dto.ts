import { IsNotEmpty, IsString } from "class-validator";
import { classToPlain } from 'class-transformer';
import { IdeaEntity } from "src/entities/idea.entity";  
import { ApiProperty } from "@nestjs/swagger";

export class UserDTO {
    @IsString()
    @ApiProperty({type: String, description: 'email'})
    username: string;
    @IsString()
    @ApiProperty({type: String, description: 'password'})
    password: string;
}
export class UserRO {
    id: string;
    username: string;
    token?: string;
    ideas? : IdeaEntity[];
    bookmarks? : IdeaEntity[];
}