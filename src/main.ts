import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import 'dotenv/config';
import { Logger } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule} from '@nestjs/swagger'
import { access } from 'fs';
const port = process.env.PORT;
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const options = new DocumentBuilder()
  .setTitle('Bookshop API')
  .setDescription('Bookshop API')
  .addBearerAuth({
    type: 'http', scheme: 'json',
    bearerFormat: 'Token'
  }, 'access-token').setVersion('1.0.0').build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  await app.listen(port || 4000);
  Logger.log(`Server running on htttp://localhost:${port}`);
}
bootstrap();
